# Печать из командной строки Windows

rundll32 C:\WINDOWS\system32\shimgvw.dll,ImageView_PrintTo "АБСОЛЮТНЫЙ_ПУТЬ_К_ФАЙЛУ" "ИМЯ_ПРИНТЕРА"

Документация: _http://techsupt.winbatch.com/webcgi/webbatch.exe?techsupt/tsleft.web+Tutorials+Printing.txt_
Подробный пример находится в низу страницы.


# Другие способы печати
## Mac
С помощью команды lp.
Мануал: _https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man1/lp.1.html_

## Windows
_mspaint /p ПУТЬ_К_ФАЙЛУ /pt "ИМЯ_СЕТЕВОГО_ПРИНТЕРА"_ печатает с помощью установленной по умолчанию Paint. При печати будет мелькать маленькое служебное окно.

_mspaint /pt ПУТЬ_К_ФАЙЛУ_ печатает в принтер по умолчанию.

В обоих случаях, при указании абсолютных путей, нужно заключать путь в кавычки, особенно, если в пути есть пробелы.
